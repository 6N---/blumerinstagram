import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:photo_manager/photo_manager.dart';



class GetPhotosData extends StateNotifier<List<AssetEntity>> {
  GetPhotosData(List<AssetEntity> state) : super([]);

  void estado(List<AssetEntity> foto) {
    state = foto;
  }

  void add(AssetEntity foto) {
    state = [...state, foto];
  }
}
