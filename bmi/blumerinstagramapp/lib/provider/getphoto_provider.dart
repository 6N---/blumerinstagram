import 'package:blumer_instagram_app/services/getphoto_service.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';


final getPhotoProvider = StateNotifierProvider((ref) => GetPhotosData([]));
