import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MultipleIcono extends StatefulWidget {
  const MultipleIcono({Key? key}) : super(key: key);

  @override
  _MultipleIconoState createState() => _MultipleIconoState();
}

class _MultipleIconoState extends State<MultipleIcono> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        SizedBox(
          width: 15.0,
          height: 15.0,
          child: SvgPicture.string(
            '<svg viewBox="257.3 3528.6 20.0 20.0" ><path transform="translate(255.31, 3526.61)" d="M 20 4 L 20 16 L 22 16 L 22 2 L 8 2 L 8 4 L 20 4 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /><path transform="translate(255.31, 3526.61)" d="M 2 8 L 2 22 L 16 22 L 16 8 L 2 8 Z M 14 10 L 4 10 L 4 20 L 14 20 L 14 10 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /><path transform="translate(255.31, 3526.61)" d="M 17 7 L 5 7 L 5 5 L 19 5 L 19 19 L 17 19 L 17 7 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>',
            allowDrawingOutsideViewBox: true,
          ),
        ),
      ],
    );
  }
}
