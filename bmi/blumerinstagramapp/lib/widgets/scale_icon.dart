import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ScaleIcono extends StatefulWidget {
  const ScaleIcono({Key? key}) : super(key: key);

  @override
  _ScaleIconoState createState() => _ScaleIconoState();
}

class _ScaleIconoState extends State<ScaleIcono> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        SizedBox(
          width: 16.0,
          height: 16.0,
          child: SvgPicture.string(
            '<svg viewBox="758.5 2534.1 16.0 16.0" ><path transform="translate(754.51, 2530.07)" d="M 4 14 L 4 20 L 10 20 L 10 18 L 6 18 L 6 14 L 4 14 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /><path transform="translate(754.51, 2530.07)" d="M 9 9 L 9 15 L 15 15 L 15 9 L 9 9 Z M 13 11 L 11 11 L 11 13 L 13 13 L 13 11 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /><path transform="translate(754.51, 2530.07)" d="M 4 10 L 4 4 L 10 4 L 10 6 L 6 6 L 6 10 L 4 10 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /><path transform="translate(754.51, 2530.07)" d="M 20 10 L 20 4 L 14 4 L 14 6 L 18 6 L 18 10 L 20 10 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /><path transform="translate(754.51, 2530.07)" d="M 20 14 L 20 20 L 14 20 L 14 18 L 18 18 L 18 14 L 20 14 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>',
            allowDrawingOutsideViewBox: true,
          ),
        ),
      ],
    );
  }
}
