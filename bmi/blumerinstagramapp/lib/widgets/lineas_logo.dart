import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LineasLogo extends StatefulWidget {
  const LineasLogo({Key? key}) : super(key: key);

  @override
  _LineasLogoState createState() => _LineasLogoState();
}

class _LineasLogoState extends State<LineasLogo> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        SizedBox(
          width: 376.0,
          height: 483.0,
          child: SvgPicture.string(
            '<svg viewBox="0.0 46.0 375.5 483.0" ><path transform="translate(124.5, 46.0)" d="M 0 0 L 0 483.0138549804688" fill="none" stroke="#707070" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /><path transform="translate(250.5, 46.0)" d="M 0 0 L 0 483.0138549804688" fill="none" stroke="#707070" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /><path transform="translate(0.0, 192.5)" d="M 0 0 L 375.5 0" fill="none" stroke="#707070" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /><path transform="translate(0.0, 359.5)" d="M 0 0 L 375.5 0" fill="none" stroke="#707070" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>',
            allowDrawingOutsideViewBox: true,
          ),
        ),
      ],
    );
  }
}
