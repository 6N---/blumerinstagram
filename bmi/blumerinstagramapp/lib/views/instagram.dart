import 'dart:developer';
import 'dart:typed_data';

import 'package:blumer_instagram_app/provider/getphoto_provider.dart';
import 'package:blumer_instagram_app/utils.dart/colores.dart';
import 'package:blumer_instagram_app/utils.dart/size_config.dart';
import 'package:blumer_instagram_app/views/gallery.dart';
import 'package:blumer_instagram_app/widgets/camara_icon.dart';
import 'package:blumer_instagram_app/widgets/equiz_icon.dart';
import 'package:blumer_instagram_app/widgets/lineas_logo.dart';
import 'package:blumer_instagram_app/widgets/multiple_icon.dart';
import 'package:blumer_instagram_app/widgets/scale_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:photo_manager/photo_manager.dart';
import 'package:photo_view/photo_view.dart';

List<AssetEntity> _imageList = [];
class Instagram extends StatefulWidget {
  static const String routeName = 'intro';
  const Instagram({Key? key}) : super(key: key);

  @override
  _InstagramState createState() => _InstagramState();
}

class _InstagramState extends State<Instagram> {
  TransformationController controllerT = TransformationController();
  PhotoViewScaleStateController scaleStateController =
      PhotoViewScaleStateController();
  PhotoViewController controller = PhotoViewController();
  double? scaleCopy;
  String urlImage =
      "https://i2.wp.com/codigoespagueti.com/wp-content/uploads/2021/05/Marvel_-Este-es-el-ranking-de-poder-de-los-superheroes-en-el-MCU.jpg?resize=1280%2C720&quality=80&ssl=1";

  String urlImage1 = "";
  bool bandera1 = true;

  List<AssetEntity> assets = [];
  int currentPage = 0;
  List<Widget> _mediaList = [];
  int? lastPage;

  var imagenFirst;

  @override
  void initState() {
        super.initState();
    scaleStateController = PhotoViewScaleStateController();
  }

  void goBack() {
    if (bandera1) {
      scaleStateController.scaleState = PhotoViewScaleState.values[1];
      setState(() {
        bandera1 = false;
      });
    } else {
      scaleStateController.scaleState = PhotoViewScaleState.initial;
      setState(() {
        bandera1 = true;
      });
    }
  }

  @override
  void dispose() {
    scaleStateController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          title: const Text("Nueva publicación"),
          centerTitle: true,
          leading: SizedBox(
            width: 40,
            height: 40,
            child: Row(
              children: const [
                Padding(
                  padding: EdgeInsets.only(left: 20.0),
                  child: EquizIcono(),
                ),
              ],
            ),
          ),
          backgroundColor: backGround,
          actions: const [
            Center(
              child: Padding(
                padding: EdgeInsets.only(right: 20),
                child: Text(
                  "Siguiente",
                  style: TextStyle(color: Colors.blue),
                ),
              ),
            )
          ],
        ),
        body: Column(
          children: [
            Expanded(
                child: Stack(
              children: [
                ClipRect(
                  child: PhotoView(
                      imageProvider: const AssetImage("assets/sunset.jpg"),
                      loadingBuilder: (context, progress) => const Center(
                            child: SizedBox(
                              width: 20.0,
                              height: 20.0,
                              child: CircularProgressIndicator(
                                color: Colors.white,
                              ),
                            ),
                          ),
                      scaleStateController: scaleStateController,
                      backgroundDecoration:
                          const BoxDecoration(color: Colors.black),
                      /* gaplessPlayback: true, */
                      enablePanAlways: true,
                      customSize: MediaQuery.of(context).size,
                      //scaleStateChangedCallback: (value) => 10,
                      enableRotation: false,
                      /*  controller: controller, */
                      minScale: 0.1,
                      maxScale: 1.5,
                      initialScale: PhotoViewComputedScale.contained,
                      basePosition: Alignment.center,
                      scaleStateCycle: defaultScaleStateCycle),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Container(
                      alignment: Alignment.bottomLeft,
                      child: GestureDetector(
                        onTap: () {
                          goBack();
                        },
                        child: Container(
                            width: 40,
                            height: 40,
                            /*  color: Colors.grey.withOpacity(0.2), */
                            child: const Center(child: ScaleIcono()),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.grey.withOpacity(0.2))),
                      )),
                )
              ],
            )),
            Expanded(
                child: Column(
              children: [
                Expanded(
                  flex: 1,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: GestureDetector(
                          onTap: () {},
                          child: Row(
                            children: const [
                              Text(
                                "Recientes",
                                style: TextStyle(color: Colors.white),
                              ),
                              Icon(
                                Icons.arrow_drop_down,
                                color: Colors.white,
                                size: 20.0,
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: Row(
                          children: [
                            GestureDetector(
                              onTap: () {},
                              child: Container(
                                  width: 30,
                                  height: 30,
                                  /*  color: Colors.grey.withOpacity(0.2), */
                                  child: const Center(child: MultipleIcono()),
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.grey.withOpacity(0.2))),
                            ),
                            const SizedBox(
                              width: 30,
                            ),
                            GestureDetector(
                              onTap: () {},
                              child: Container(
                                  width: 30,
                                  height: 30,
                                  /*  color: Colors.grey.withOpacity(0.2), */
                                  child: const Center(child: CamaraIcono()),
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.grey.withOpacity(0.2))),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                    flex: 5,
                    child: Container(
                      color: Colors.yellow,
                      child: const PhotoPicker(),
                    ))
              ],
            ))
          ],
        ),
      ),
    );
  }
}

class PhotoPicker extends StatefulWidget {
  const PhotoPicker({Key? key}) : super(key: key);

  @override
  _PhotoPickerState createState() => _PhotoPickerState();
}

class _PhotoPickerState extends State<PhotoPicker> {
  List<Widget> _photoList = [];
  List<AssetEntity> _selectedList = [];

  int currentPage = 0;
  int lastPage = 0;
  int maxSelection = 5;
  bool firstimagen = true;
  _handleScrollEvent(ScrollNotification scroll) {
    if (scroll.metrics.pixels / scroll.metrics.maxScrollExtent > 0.33) {
      if (currentPage != lastPage) {
        _fetchPhotos();
      }
    }
  }

  _fetchPhotos() async {
    lastPage = currentPage;
    var result = await PhotoManager.requestPermission();
    if (result) {
      //load the album list
      List<AssetPathEntity> albums = await PhotoManager.getAssetPathList(
          onlyAll: true, type: RequestType.image);
      List<AssetEntity> media =
          await albums[0].getAssetListPaged(currentPage, 60);
      List<Widget> temp = [];
      for (var asset in media) {
        /* if (firstimagen) {
          Consumer(builder: (context, wath, _) {
            return Container();
          });

          context.read(getPhotoProvider.notifier).estado([asset]);
          // ignore: invalid_use_of_protected_member
          inspect(context.read(getPhotoProvider.notifier).state);
          setState(() {
            _imageList = context.read(getPhotoProvider.notifier).state;
          });

          firstimagen = false;
        } */

        temp.add(
          PhotoPickerItem(
              asset: asset,
              onSelect: (AssetEntity asset, bool selected) {
                // selected is the current selection state, so be for touch
                if (selected) {
                  _selectedList.remove(asset);
                  return !selected;
                } else if (_selectedList.length < maxSelection) {
                  _selectedList.add(asset);

                  return !selected;
                }

                return selected;
              }),
        );
      }
      setState(() {
        _photoList.addAll(temp);
        //aqui

        currentPage++;
      });
    } else {
      // fail
      /// if result is fail, you can call `PhotoManager.openSetting();`  to open android/ios applicaton's setting to get permission
    }
  }

  @override
  void initState() {
    super.initState();
    _fetchPhotos();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        /*  appBar: AppBar(
          title: Text("Photo picker"),
          actions: <Widget>[
            FlatButton(
              onPressed: () {
                Navigator.of(context).pop(_selectedList);
              },
              child: Text('Done'),
              textColor: Colors.white,
            )
          ],
        ), */
        body: NotificationListener<ScrollNotification>(
      onNotification: (ScrollNotification scroll) {
        _handleScrollEvent(scroll);
        return true;
      },
      child: GridView.builder(
          itemCount: _photoList.length,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 4, crossAxisSpacing: 2, mainAxisSpacing: 2),
          itemBuilder: (BuildContext context, int index) {
            return _photoList[index];
          }),
    ));
  }
}

class PhotoPickerItem extends StatefulWidget {
  final Key? key;
  final AssetEntity asset;
  final bool Function(AssetEntity asset, bool isSelected) onSelect;

  const PhotoPickerItem(
      {required this.asset, required this.onSelect, this.key});

  @override
  _PhotoPickerItemState createState() => _PhotoPickerItemState();
}

class _PhotoPickerItemState extends State<PhotoPickerItem> {
  bool isSelected = false;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Uint8List?>(
      future: widget.asset.thumbDataWithSize(200, 200),
      builder: (BuildContext context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return GestureDetector(
            onTap: () {
              setState(() {
                print("Valor" + isSelected.toString());
                // isSelected = !isSelected;

                if (isSelected) {
                  /*  Consumer(builder: (context, wath, _) {
                    return Container();
                  });

                  context.read(getPhotoProvider.notifier).estado([]);
                  // ignore: invalid_use_of_protected_member
                  inspect(context.read(getPhotoProvider.notifier).state);  */
                } else {
                  context
                      .read(getPhotoProvider.notifier)
                      .estado([widget.asset]);
                  // ignore: invalid_use_of_protected_member
                  setState(() {
                    _imageList =
                        (context.read(getPhotoProvider.notifier).state);
                  });
                }

                isSelected = widget.onSelect(widget.asset, isSelected);
              });
            },
            child: Stack(
              children: <Widget>[
                Positioned.fill(
                  child: Image.memory(
                    snapshot.data!,
                    fit: BoxFit.cover,
                  ),
                ),
                if (isSelected)
                  Positioned.fill(
                    child: Container(
                      color: Colors.white.withOpacity(0.5),
                    ),
                  ),
                if (isSelected)
                  const Align(
                    alignment: Alignment.bottomRight,
                    child: Padding(
                      padding: EdgeInsets.only(right: 5, bottom: 5),
                      child: Icon(
                        Icons.fiber_manual_record,
                        color: Colors.blue,
                      ),
                    ),
                  ),
                if (isSelected)
                  const Align(
                    alignment: Alignment.bottomRight,
                    child: Padding(
                      padding: EdgeInsets.only(right: 5, bottom: 5),
                      child: Icon(
                        Icons.check_circle_outline,
                        color: Colors.white,
                      ),
                    ),
                  ),
              ],
            ),
          );
        }
        return Container();
      },
    );
  }
}
